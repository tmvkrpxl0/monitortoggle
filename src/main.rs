use std::error::Error;
use std::process::{Command, Stdio};
use std::thread;
use std::time::Duration;
use dbus::ffidisp::Connection;

use ksni::{Category, Icon, Tray, TrayService};
use simple_error::{simple_error, SimpleError};

#[derive(Clone)]
struct MonitorState {
    monitors: Vec<(String, bool)>,
    primary: String,
}

#[derive(Clone)]
struct ToggleTray {
    monitor: MonitorState,
    enabled: Icon,
    disabled: Icon,
}

impl Tray for ToggleTray {
    fn activate(&mut self, _x: i32, _y: i32) {
        if self.is_enabled() {
            self.monitor.monitors.iter().for_each(|(name, is_active)| {
                if name != &self.monitor.primary && *is_active {
                    turn_off_monitor(name).unwrap()
                }
            })
        } else {
            self.monitor.monitors.iter().for_each(|(name, is_active)| {
                if !*is_active {
                    turn_on_monitor(name).unwrap()
                }
            })
        }

        self.monitor = compute_monitors().unwrap();
    }


    fn category(&self) -> Category {
        Category::Hardware
    }

    fn icon_pixmap(&self) -> Vec<Icon> {
        if self.is_enabled() {
            vec![self.enabled.clone()]
        } else {
            vec![self.disabled.clone()]
        }
    }
}

impl ToggleTray {
    fn is_enabled(&self) -> bool {
        self.monitor.monitors.iter().filter(|(_, e)| *e).count() >= 2
    }
}

fn main() {
    let (enabled, disabled) = {
        let raw = include_bytes!("img.png");

        let image = image::load_from_memory(raw).unwrap();
        println!("Loaded raw img file");
        let (width, height) = (image.width(), image.height());
        let rgba = image.into_rgba8();

        let enabled: Vec<_> = rgba.chunks(4).map(|pixel| {
            [pixel[3], pixel[0], pixel[1], pixel[2]] // why it's ARGB32...
        }).collect::<Vec<_>>().concat();
        let disabled = enabled.iter().map(|v| *v / 2).collect();

        let enabled = Icon {
            width: width as i32,
            height: height as i32,
            data: enabled,
        };

        let disabled = Icon {
            width: width as i32,
            height: height as i32,
            data: disabled,
        };

        (enabled, disabled)
    };
    println!("Icon has been processed, computing monitor states...");

    let tray = ToggleTray {
        monitor: compute_monitors().expect("Unable to compute monitor states!"),
        enabled,
        disabled,
    };

    wait_for_dbus();
    println!("Dbus detected, Launching service...");
    loop {
        let service = TrayService::new(tray.clone());
        if service.run().is_ok() {
            break
        }
    };
}

fn wait_for_dbus() {
    while Connection::new_session().is_err() {
        println!("Dbus not ready...");
        thread::sleep(Duration::from_secs(2))
    }
}

fn turn_on_monitor(name: &str) -> Result<(), Box<dyn Error>> {
    // TODO Maybe de-hard-code this?
    run_command(&format!("xrandr --output {} --auto --right-of HDMI-0", name))?;
    Ok(())
}

fn turn_off_monitor(name: &str) -> Result<(), Box<dyn Error>> {
    run_command(&format!("xrandr --output {} --off", name))?;
    Ok(())
}

fn run_command(command: &str) -> Result<String, Box<dyn Error>> {
    let mut split = command.split(' ');
    let output = {
        let mut command = Command::new(split.next().unwrap());
        split.for_each(|arg| { command.arg(arg); });
        command
    }.stdout(Stdio::piped())
        .output()?;

    if !output.status.success() {
        return Err(Box::new(simple_error!("Command {} failed with status {}\n{}",
                    command,
                    output.status.code().unwrap(),
                    String::from_utf8_lossy(&output.stderr)
        )));
    };

    let stdout = String::from_utf8_lossy(&output.stdout).to_string();
    Ok(stdout)
}

fn compute_monitors() -> Result<MonitorState, Box<dyn Error>> {
    let mut error: Option<SimpleError> = None;

    let all_monitors: Vec<_> = {
        let output = run_command("xrandr")?;
        output.lines().filter(|line| {
            line.contains("connected") && !line.contains("disconnected")
        }).map_while(|line| {
            let first_space = line
                .bytes()
                .position(|b| b == b' ');

            match first_space {
                None => {
                    error = Some(simple_error!("Invalid line from xrandr!\n{}", line));
                    None
                }
                Some(first_space) => {
                    let name = &line[..first_space];

                    Some(name.to_owned())
                }
            }
        }).collect()
    };
    if let Some(err) = error {
        return Err(Box::new(err));
    }
    println!("Fetched all available monitors");

    let (active_monitors, primary): (Vec<String>, String) = {
        let output = run_command("xrandr --listactivemonitors")?;
        let mut lines = output.lines();
        lines.next();

        let mut primary: Option<String> = None;
        let monitors = lines.map_while(|line| {
            let last_space = line
                .bytes()
                .rposition(|b| b == b' ');

            match last_space {
                None => {
                    error = Some(simple_error!("Invalid line from xrandr!\n{}", line));
                    None
                }
                Some(last_space) => {
                    let name = &line[last_space + 1..];
                    if line.contains('*') {
                        primary = Some(name.to_string());
                    };
                    Some(name.to_string())
                }
            }
        }).collect();

        // Ah screw it, I'm not generalizing this for other systems
        // I physically only have 2 hdmi port, I can just hard code names, why am I doing this?
        let primary = "HDMI-0".to_string();

        (monitors, primary)
    };
    if let Some(err) = error {
        return Err(Box::new(err));
    }
    println!("Figured out which ones are primary and active");

    let monitors = all_monitors.into_iter().map(|monitor| {
        let is_active = active_monitors.contains(&monitor);
        (monitor, is_active)
    }).collect();

    Ok(MonitorState {
        monitors,
        primary,
    })
}
